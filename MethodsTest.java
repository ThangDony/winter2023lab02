public class MethodsTest {
	
	public static void main(String[] args) {
		
		int x = 5;
		/* 
		methodNoInputNoReturn();
		
		methodOneInputNoReturn(x+10);
				
		methodTwoInputNoReturn(1,1.0);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		System.out.println(sumSquareRoot(9,5));
		
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		
		System.out.println(SecondClass.addOne(50));
		System.out.println(SecondClass.addTwo(50));
		*/
		
		SecondClass sc = new SecondClass();
		
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn() {
		
		System.out.println("Inside method that takes no input and returns nothing");
		
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int intNumber) {
		
		System.out.println("Inside the method one input no return");
		
		System.out.println(intNumber);
	}
	
	public static void methodTwoInputNoReturn(int intNumber, double doubleNumber) {
		
		System.out.println("Inside the method two inputs no return");
		System.out.println(intNumber);
		System.out.println(doubleNumber);
		
	}
	
	public static int methodNoInputReturnInt() {
		
		System.out.println("Inside the method no input, return Int");
		return 5;
		
	}
	
	public static double sumSquareRoot(int number1, int number2) {

		System.out.println("Inside the method sumSquareRoot");	
		
		int number3 = number1 + number2;
		
		double result = Math.sqrt(number3);
		
		return result;
		
	}
	
	
}