import java.util.Scanner;
public class PartThree {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Input first number");
		int number1 = scan.nextInt();
		System.out.println("Input second number");
		int number2 = scan.nextInt();
		
		Calculator sc = new Calculator();
		
		System.out.println(sc.add(number1, number2));
		System.out.println(sc.substract(number1, number2));
		System.out.println(sc.multiply(number1, number2));
		System.out.println(sc.divide(number1, number2));
		
	}
	
}